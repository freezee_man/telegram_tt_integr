from django.db import models


class VkApi(models.Model):
    vk_access_code = models.CharField(max_length=200)

    def __str__(self):
        return self.vk_access_code


class Vkpost_video(models.Model):
    vk_owner_id = models.CharField(max_length=200)
    vk_video_id = models.CharField(max_length=200)
    vk_video_720 = models.CharField(max_length=200)
    vk_video_480 = models.CharField(max_length=200)
    vk_video_240 = models.CharField(max_length=200)
    # date_parsed = models.DateTimeField('date parsed')
    vk_access_code = models.ForeignKey(VkApi, on_delete=models.CASCADE)

    def quality_720(self):
        return self.vk_video_720

    def quality_480(self):
        return self.vk_video_480

    def quality_240(self):
        return self.vk_video_240

    def __str__(self):
        return self.vk_owner_id + '_' + self.vk_video_id
